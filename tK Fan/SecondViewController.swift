//
//  SecondViewController.swift
//  tK Fan
//
//  Created by timmcewan on 11/15/16.
//  Copyright © 2016 breakthelabel. All rights reserved.
//
import UIKit

class SecondViewController: UIViewController, UITableViewDelegate, UITableViewDataSource  {
    @IBOutlet weak var tableview: UITableView!
    var manager = [Managers]()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableview.dataSource = self
        self.tableview.delegate = self
        // Do any additional setup after loading the view, typically from a nib.
        self.tableview.reloadData()
        loadmembers()
        filterList()
        print(manager.count)
    }
    func loadmembers() {
        let kosdff = Managers(name: "kosdff", job: "Owner", twitter: "KOSDFF", twitterapp: "KOSDFF", twitch: "kosdff", twitchapp: "kosdff", instagram: "KOSDFF", instagramapp: "KOSDFF", snapchat: "KOSDFF", snapchatapp: "KOSDFF", youtube: "xKOSDFF", youtubeINTRO: "zPxEjQ13JBA", youtubeapp: "xKOSDFF")
        kosdff.frontimage = UIImage(named: "KOSDFF")!
        kosdff.bottomhero = UIImage(named: "kosdffimage")!
        manager.append(kosdff)
        let hammy = Managers(name: "hamz", job: "General Manager", twitter: "HAMIZMYNAME", twitterapp: "HAMIZMYNAME", twitch: "gtimabouttogoham", twitchapp: "gtimabouttogoham", instagram: " ", instagramapp: " ", snapchat: " ", snapchatapp: " ", youtube: "GTimABOUTtogoHAM", youtubeINTRO: "BA8aF6todjc", youtubeapp: "GTimABOUTtogoHAM")
        hammy.frontimage = UIImage(named: "HAMZ")!
        hammy.bottomhero = UIImage(named: "hamhero")!
        manager.append(hammy)
        let samurai = Managers(name: "samurai", job: "Lead Creative Director", twitter: "Samurai_tK", twitterapp: "Samurai_tK", twitch: "xsmri", twitchapp: "xsmri" , instagram: " ", instagramapp: " ", snapchat: " ", snapchatapp: " ", youtube: "xSMRI", youtubeINTRO: "CScl2ESFAA0", youtubeapp: "xSMRI")
        samurai.frontimage = UIImage(named: "samurai")!
        manager.append(samurai)
    }
    func filterList() { // should probably be called sort and not filter
        manager.sort() { $1.name > $0.name } // sort the fruit by name
        // notify the table view the data has changed
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return manager.count
    }
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        let manager = self.manager[indexPath.row]
        cell.textLabel?.text = manager.name
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let managers = manager[(indexPath as NSIndexPath).row]
        self.performSegue(withIdentifier: "ManagerSegue", sender: managers)
        
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
       let ControlME = segue.destination as! ThirdViewController
        ControlME.manager = sender as! Managers

    }
}

