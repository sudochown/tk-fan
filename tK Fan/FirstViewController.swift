//
//  FirstViewController.swift
//  tK Fan
//
//  Created by timmcewan on 11/15/16.
//  Copyright © 2016 breakthelabel. All rights reserved.
//

import UIKit
import TwitterKit
class FirstViewController: TWTRTimelineViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let client = TWTRAPIClient()
        self.dataSource = TWTRUserTimelineDataSource(screenName: "TeamKaliber", apiClient: client)
        TWTRTweetView.appearance().primaryTextColor = UIColor.white
        TWTRTweetView.appearance().backgroundColor = UIColor.init(red: 19/255.0, green: 19/255.0, blue: 19/255.0, alpha: 1.0)
        TWTRTweetView.appearance().linkTextColor = UIColor.red

        
        self.dataSource = TWTRUserTimelineDataSource(screenName: "kalibercreative", apiClient: client)
        TWTRTweetView.appearance().primaryTextColor = UIColor.white
        TWTRTweetView.appearance().backgroundColor = UIColor.init(red: 19/255.0, green: 19/255.0, blue: 19/255.0, alpha: 1.0)
        TWTRTweetView.appearance().linkTextColor = UIColor.red
        
}

}
