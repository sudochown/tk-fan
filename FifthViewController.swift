//
//  FifthViewController.swift
//  tK Fan
//
//  Created by timmcewan on 11/18/16.
//  Copyright © 2016 breakthelabel. All rights reserved.
//

//
//  ThirdViewController.swift
//  tK Fan
//
//  Created by timmcewan on 11/15/16.
//  Copyright © 2016 breakthelabel. All rights reserved.
//

import UIKit
import Foundation
import MediaPlayer
import Crashlytics

class MyRequestController {

}






class FifthViewController: UIViewController {
    var member = Members()
    
    @IBOutlet weak var LiveNOWOUTLET: UIButton!
    @IBOutlet weak var profilepicture: UIImageView!
    @IBOutlet weak var heroimage: UIImageView!
    @IBOutlet weak var longimage: UIImageView!
    @IBOutlet weak var youtuberect: UIImageView!

    
    @IBOutlet weak var BackgroundHeroImage: UIImageView!
    @IBOutlet weak var wv: UIWebView!
    @IBOutlet weak var NAMEOUTLET: UILabel!
    // MARK: - Social Handles
    @IBOutlet weak var twitch: UIButton!
    @IBOutlet weak var youtube: UIButton!
    @IBOutlet weak var twitter: UIButton!
    @IBOutlet weak var instagram: UIButton!
    @IBOutlet weak var snapchat: UIButton!
    
    @IBOutlet weak var jobOUTLET: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()




        

        self.profilepicture.image = self.member.frontimage
        profilepicture.layer.borderWidth = 2
        profilepicture.layer.masksToBounds = false
        profilepicture.layer.cornerRadius = profilepicture.frame.height/4
        profilepicture.clipsToBounds = true

        

        self.heroimage.image = self.member.bottomhero
        self.NAMEOUTLET.text = self.member.name.uppercased()
        self.jobOUTLET.text = self.member.job.lowercased()
        loadYoutube(videoID: self.member.youtubeINTRO)
        
        // ————————————————————————————————————————————————————————
        // MARK: - Social Media Hide or not

        
        
        self.twitch.isHidden = self.member.twitch.contains("http://player.twitch.tv/?channel= ")
        self.twitch.isHidden = self.member.twitchapp.contains("twitch://stream/#channel_name ")
        self.youtube.isHidden = self.member.youtube.contains("https://www.youtube.com/user/ ")
        self.youtube.isHidden = self.member.youtubeapp.contains("youtube://www.youtube.com/ ")
        self.twitter.isHidden = self.member.twitter.contains("https://twitter.com/ ")
        self.twitter.isHidden = self.member.twitterapp.contains("twitter://user?screen_name= ")
        self.instagram.isHidden = self.member.instagramapp.contains("instagram://user?username= ")
        self.instagram.isHidden = self.member.instagram.contains("https://instagram.com/ ")
        self.snapchat.isHidden = self.member.snapchat.contains("https://www.snapchat.com/add/ ")
        self.wv.isHidden = self.member.youtubeINTRO.contains(" ")
        

    }


     
    
    
    // MARK: - Functions
    func loadYoutube(videoID:String) {
        // create a custom youtubeURL with the video ID
        guard
            let youtubeURL = NSURL(string: "https://www.youtube.com/embed/\(videoID)")
            else { return }
        // load your web request
        wv.loadRequest( NSURLRequest(url: youtubeURL as URL) as URLRequest )
        
        
        // ————————————————————————————————————————————————————————
    }

    @IBAction func twitter(_ sender: Any) {
        let url2 = NSURL(string: self.member.twitterapp)!
        let url = NSURL(string: self.member.twitter)!
        if UIApplication.shared.canOpenURL(url2 as URL){
            
        }
        //            IF THE TOP FAILS IT GOES TO THE BOTTOMS AND OPENS IN APP
        UIApplication.shared.open(url as URL, options: [:], completionHandler: nil)
        print("HELLO")
        //redirect to safari because the user doesn't have Twitter
        UIApplication.shared.open(url2 as URL, options: [:], completionHandler: nil)
    }
    
    @IBAction func instagramme(_ sender: Any) {
        let instagramfail = NSURL(string: self.member.instagram)!
        let instagrammer = NSURL(string: self.member.instagramapp)!
        if UIApplication.shared.canOpenURL(instagrammer as URL){
            
        }
        //            IF THE TOP FAILS IT GOES TO THE BOTTOMS AND OPENS IN APP
        UIApplication.shared.open(instagramfail as URL, options: [:], completionHandler: nil)
        print("HELLO")
        //redirect to safari because the user doesn't have Twitter
        UIApplication.shared.open(instagrammer as URL, options: [:], completionHandler: nil)
        
    }
    

    
    
    
    @IBAction func snapchatbutton(_ sender: Any) {
        let url2 = NSURL(string: self.member.snapchatapp)!
        let url = NSURL(string: self.member.snapchat)!
        if UIApplication.shared.canOpenURL(url2 as URL){
            
        }
        //            IF THE TOP FAILS IT GOES TO THE BOTTOMS AND OPENS IN APP
        UIApplication.shared.open(url as URL, options: [:], completionHandler: nil)
        print("HELLO")
        //redirect to safari because the user doesn't have Twitter
        UIApplication.shared.open(url2 as URL, options: [:], completionHandler: nil)
    }
    
    @IBAction func youtubeButton(_ sender: Any) {
        
        let url2 = NSURL(string: self.member.youtubeapp)!
        let url = NSURL(string: self.member.youtube)!
        if UIApplication.shared.canOpenURL(url2 as URL){
            
        }
        //            IF THE TOP FAILS IT GOES TO THE BOTTOMS AND OPENS IN APP
        UIApplication.shared.open(url as URL, options: [:], completionHandler: nil)
        print("HELLO")
        //redirect to safari because the user doesn't have Twitter
        UIApplication.shared.open(url2 as URL, options: [:], completionHandler: nil)
    }
    
    @IBAction func twitchButton(_ sender: Any) {
        let url2 = NSURL(string: self.member.twitchapp)!
        let url = NSURL(string: self.member.twitch)!
        if UIApplication.shared.canOpenURL(url2 as URL){
            
        }
        //            IF THE TOP FAILS IT GOES TO THE BOTTOMS AND OPENS IN APP
        UIApplication.shared.open(url as URL, options: [:], completionHandler: nil)
        print("HELLO")
        //redirect to safari because the user doesn't have Twitter
        UIApplication.shared.open(url2 as URL, options: [:], completionHandler: nil)

    }
    
}
