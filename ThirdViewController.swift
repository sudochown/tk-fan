//
//  ThirdViewController.swift
//  tK Fan
//
//  Created by timmcewan on 11/15/16.
//  Copyright © 2016 breakthelabel. All rights reserved.
//

import UIKit
import Foundation
import MediaPlayer

class ThirdViewController: UIViewController {
    var manager = Managers()

    

    @IBOutlet weak var heroimage: UIImageView!
    @IBOutlet weak var wv: UIWebView!
    @IBOutlet weak var NAMEOUTLET: UILabel!
    @IBOutlet weak var profilepicture: UIImageView!
    
    @IBOutlet weak var longimage: UIImageView!
    
    
    // MARK: - Social Handles
    @IBOutlet weak var twitch: UIButton!
    @IBOutlet weak var youtube: UIButton!
    @IBOutlet weak var twitter: UIButton!
    @IBOutlet weak var instagram: UIButton!
    @IBOutlet weak var snapchat: UIButton!
    
    @IBOutlet weak var jobOUTLET: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.twitch.isHidden = self.manager.twitch.contains("https://www.twitch.tv/ ")
        self.twitch.isHidden = self.manager.twitchapp.contains("twitch://stream/#channel_name ")
        self.youtube.isHidden = self.manager.youtube.contains("https://www.youtube.com/user/ ")
        self.youtube.isHidden = self.manager.youtubeapp.contains("youtube.com/ ")
        self.twitter.isHidden = self.manager.twitter.contains("http://twitter.com/ ")
        self.twitter.isHidden = self.manager.twitterapp.contains("twitter://user?screen_name= ")
        self.instagram.isHidden = self.manager.instagramapp.contains("user?username= ")
        self.instagram.isHidden = self.manager.instagram.contains("https://instagram.com/ ")
        self.snapchat.isHidden = self.manager.snapchat.contains("https://www.snapchat.com/add/ ")
        self.wv.isHidden = self.manager.youtubeINTRO.isEmpty
        
        

        loadYoutube(videoID: self.manager.youtubeINTRO)
        self.profilepicture.image = self.manager.frontimage
        profilepicture.layer.borderWidth = 2
        profilepicture.layer.masksToBounds = false
        profilepicture.layer.cornerRadius = profilepicture.frame.height/4
        profilepicture.clipsToBounds = true

        
        
        self.heroimage.image = self.manager.bottomhero
        self.NAMEOUTLET.text = self.manager.name.uppercased()
        self.jobOUTLET.text = self.manager.job.lowercased()
        
        
// ————————————————————————————————————————————————————————
        // MARK: - Social Media Hide or not

        }
// ————————————————————————————————————————————————————————
    
    // MARK: - Functions
    func loadYoutube(videoID:String) {
        // create a custom youtubeURL with the video ID
        guard
            let youtubeURL = NSURL(string: "https://www.youtube.com/embed/\(videoID)")
            else { return }
        // load your web request
        wv.loadRequest( NSURLRequest(url: youtubeURL as URL) as URLRequest )
        
    
// ————————————————————————————————————————————————————————
    }
    
    // MARK: - Social Buttons
    @IBAction func twitter(_ sender: Any) {
        let url2 = NSURL(string: self.manager.twitterapp)!
        let url = NSURL(string: self.manager.twitter)!
        if UIApplication.shared.canOpenURL(url2 as URL){
            
        }
        //            IF THE TOP FAILS IT GOES TO THE BOTTOMS AND OPENS IN APP
        UIApplication.shared.open(url as URL, options: [:], completionHandler: nil)
        print("HELLO")
        //redirect to safari because the user doesn't have Twitter
        UIApplication.shared.open(url2 as URL, options: [:], completionHandler: nil)
    }

    
    @IBAction func instagramme(_ sender: Any) {
        let instagramfail = NSURL(string: self.manager.instagram)!
        let instagrammer = NSURL(string: self.manager.instagramapp)!
        if UIApplication.shared.canOpenURL(instagrammer as URL){
        
        }
//            IF THE TOP FAILS IT GOES TO THE BOTTOMS AND OPENS IN APP
            UIApplication.shared.open(instagramfail as URL, options: [:], completionHandler: nil)
            print("HELLO")
            //redirect to safari because the user doesn't have Twitter
            UIApplication.shared.open(instagrammer as URL, options: [:], completionHandler: nil)
        }

    
    
    
    @IBAction func snapchatbutton(_ sender: Any) {
        let url2 = NSURL(string: self.manager.snapchatapp)!
        let url = NSURL(string: self.manager.snapchat)!
        if UIApplication.shared.canOpenURL(url2 as URL){
            
        }
        //            IF THE TOP FAILS IT GOES TO THE BOTTOMS AND OPENS IN APP
        UIApplication.shared.open(url as URL, options: [:], completionHandler: nil)
        print("HELLO")
        //redirect to safari because the user doesn't have Twitter
        UIApplication.shared.open(url2 as URL, options: [:], completionHandler: nil)
    }
    
    @IBAction func youtubeButton(_ sender: Any) {
        let url2 = NSURL(string: self.manager.youtubeapp)!
        let url = NSURL(string: self.manager.youtube)!
        if UIApplication.shared.canOpenURL(url2 as URL){
            
        }
        //            IF THE TOP FAILS IT GOES TO THE BOTTOMS AND OPENS IN APP
        UIApplication.shared.open(url as URL, options: [:], completionHandler: nil)
        print("HELLO")
        //redirect to safari because the user doesn't have Twitter
        UIApplication.shared.open(url2 as URL, options: [:], completionHandler: nil)
    }

    @IBAction func twitchButton(_ sender: Any) {
        let url2 = NSURL(string: self.manager.twitchapp)!
        let url = NSURL(string: self.manager.twitch)!
        if UIApplication.shared.canOpenURL(url2 as URL){
            
        }
        //            IF THE TOP FAILS IT GOES TO THE BOTTOMS AND OPENS IN APP
        UIApplication.shared.open(url as URL, options: [:], completionHandler: nil)
        print("HELLO")
        //redirect to safari because the user doesn't have Twitter
        UIApplication.shared.open(url2 as URL, options: [:], completionHandler: nil)
    }
    
    
    
    
    
    
    
}
