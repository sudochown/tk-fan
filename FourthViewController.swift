//
//  FourthViewController.swift
//  tK Fan
//
//  Created by timmcewan on 11/18/16.
//  Copyright © 2016 breakthelabel. All rights reserved.
//

import UIKit

class FourthViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet weak var tableview: UITableView!
    var members = [Members]()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableview.delegate = self
        self.tableview.dataSource = self
        self.tableview.reloadData()
        
        
        loadmembers()
        filterList()
        // Do any additional setup after loading the view.
        
    }
    func filterList() { // should probably be called sort and not filter
        members.sort() { $1.name > $0.name } // sort the fruit by name
        // notify the table view the data has changed
    }
    func loadmembers() {
        
        
        
        let crispy = Members(name: "crispy", job: "Director/STREAMER", twitter: "crispy", twitterapp: "crispy", twitch: "CrispyConcords", twitchapp: "CrispyConcords", instagram: "Crispy", instagramapp: "Crispy", snapchat: "CrispyConcords", snapchatapp: "CrispyConcords", youtube: "ClanCrispy", youtubeINTRO: "xJcy4z_Bso0", youtubeapp: "ClanCrispy")
        crispy.frontimage = UIImage(named: "crispyprofile")!
        
        members.append(crispy)
        let andimgone = Members(name: "andimgone", job: "STREAMER/DIRECTOR", twitter: "AND_IM_GONE", twitterapp: "AND_IM_GONE", twitch: "GONE", twitchapp: "GONE", instagram: "ANDIMGONE1", instagramapp: "ANDIMGONE1", snapchat: " ", snapchatapp: " ", youtube: "GONE", youtubeINTRO: "3FapVXPTrdE", youtubeapp: "GONE")
        andimgone.frontimage = UIImage(named: "goneprofile")!
        andimgone.bottomhero = UIImage(named: "andimgonehero")!
        members.append(andimgone)
        let chaos = Members(name: "whoschaos", job: "Director", twitter: "WhosChaos", twitterapp: "WhosChaos", twitch: "whoschaosftw", twitchapp: "whoschaosftw", instagram: "whoschaosftw", instagramapp: "whoschaosftw", snapchat: " 1", snapchatapp: " ", youtube: "WhosChaos", youtubeINTRO: "xwmjv9sXinc", youtubeapp: "WhosChaos")
        chaos.frontimage = UIImage(named: "chaosprofile")!
        chaos.bottomhero = UIImage(named: "chaoshero")!
        members.append(chaos)
        //        twitter WhosChaos    instagram: whoschaosftw  twitch: whoschaosftw
        let kosdff = Members(name: "kosdff", job: "Owner", twitter: "KOSDFF", twitterapp: "KOSDFF", twitch: "kosdff", twitchapp: "kosdff", instagram: "KOSDFF", instagramapp: "KOSDFF", snapchat: "KOSDFF", snapchatapp: "KOSDFF", youtube: "xKOSDFF", youtubeINTRO: "zPxEjQ13JBA", youtubeapp: "xKOSDFF")
        kosdff.frontimage = UIImage(named: "KOSDFF")!
        kosdff.bottomhero = UIImage(named: "kosdffimage")!
        members.append(kosdff)
        let hammy = Members(name: "hamz", job: "General Manager", twitter: "HAMIZMYNAME", twitterapp: "HAMIZMYNAME", twitch: "gtimabouttogoham", twitchapp: "gtimabouttogoham", instagram: " ", instagramapp: " ", snapchat: " ", snapchatapp: " ", youtube: "GTimABOUTtogoHAM", youtubeINTRO: "BA8aF6todjc", youtubeapp: "GTimABOUTtogoHAM")
        hammy.frontimage = UIImage(named: "HAMZ")!
        hammy.bottomhero = UIImage(named: "hamhero")!
        members.append(hammy)
        let samurai = Members(name: "samurai", job: "Lead Creative Director", twitter: "Samurai_tK", twitterapp: "Samurai_tK", twitch: "xsmri", twitchapp: "xsmri" , instagram: " ", instagramapp: " ", snapchat: " ", snapchatapp: "NONE", youtube: "xSMRI", youtubeINTRO: "CScl2ESFAA0", youtubeapp: "xSMRI")
        samurai.frontimage = UIImage(named: "samurai")!
        members.append(samurai)
        let theory = Members(name: "theory", job: "CAPT COD: PLAYER", twitter: "Theory_tK", twitterapp: "Theory_tK", twitch: "TheoryCoD", twitchapp: "TheoryCoD", instagram: " ", instagramapp: " ", snapchat: " ", snapchatapp: " ", youtube: " ", youtubeINTRO: "e9d_xkmg1hs", youtubeapp: " ")
        theory.frontimage = UIImage(named: "theoryprofile")!
        //        theory.longimage = UIImage(named: "theoryhero")!
        
        members.append(theory)
        let sharp = Members(name: "sharp", job: "STREAMER/DIRECTOR", twitter: "Sharp_tK", twitterapp: "Sharp_tK", twitch: "Sharp", twitchapp: "Sharp", instagram: "KaLiBeRSharp", instagramapp: "KaLiBeRSharp", snapchat: " ", snapchatapp: " ", youtube: "TheSharptube", youtubeINTRO: "yxCJfzMUUIA", youtubeapp: "TheSharptube")
        sharp.frontimage = UIImage(named: "sharpprofile")!
        //        sharp.longimage = UIImage(named: "sharphero")!
        members.append(sharp)
        let fooya = Members(name: "fooya", job: "Streamer/Youtuber", twitter: "IFOOYA", twitterapp: "IFOOYA", twitch: "fooya", twitchapp: "fooya", instagram: "FOOYA", instagramapp: "FOOYA", snapchat: " ", snapchatapp: " ", youtube: "TheFooYa", youtubeINTRO: "5Gg97MysMUQ", youtubeapp: "TheFooYa")
        fooya.frontimage = UIImage(named: "fooya")!
        fooya.bottomhero = UIImage(named: "fooyahero")!
        members.append(fooya)
        let tylerbutler = Members(name: "tyler", job: "photographer/youtuber", twitter: "TylerButlerCo", twitterapp: "TylerButlerCo", twitch: " ", twitchapp: " ", instagram: "TylerButlerCo", instagramapp: "TylerButlerCo", snapchat: " ", snapchatapp: " ", youtube: "UCUsPsW4KANXlhGzD2fwLiQw", youtubeINTRO: " ", youtubeapp: " ")
        tylerbutler.frontimage = UIImage(named: "tylertk")!
        members.append(tylerbutler)
        let ryxn = Members(name: "ryxn", job: "Media", twitter: "RynAMedia", twitterapp: "RynAMedia", twitch: " ", twitchapp: " ", instagram: " ", instagramapp: " ", snapchat: " ", snapchatapp: " ", youtube: " ", youtubeINTRO: " ", youtubeapp: " ")
        ryxn.frontimage = UIImage(named: "ryxn")!
        members.append(ryxn)
        
        let joshtk = Members(name: "josh", job: "Graphic Designer", twitter: "DubaloDesign", twitterapp: "DubaloDesign", twitch: " ", twitchapp: " ", instagram: " ", instagramapp: " ", snapchat: " ", snapchatapp: " ", youtube: " ", youtubeINTRO: " ", youtubeapp: " ")
        joshtk.frontimage = UIImage(named: "joshtk")!
        members.append(joshtk)
        
        let lostinplace = Members(name: "lostinplace", job: "Streamer/director", twitter: "Lost1nPlace", twitterapp: "Lost1nPlace", twitch: "lost1nplace", twitchapp: "lost1nplace", instagram: " ", instagramapp: " ", snapchat: " ", snapchatapp: " ", youtube: "xXLostInPlaceXx", youtubeINTRO: "P8aGSm6QgAE", youtubeapp: "xXLostInPlaceXx")
        lostinplace.frontimage = UIImage(named: "lostinplace")!
        lostinplace.bottomhero = UIImage(named: "lostinhero")!
        members.append(lostinplace)
        
        let crazyteddy = Members(name: "icrazyteddy", job: "Streamer/director", twitter: "iCrazyTeddy", twitterapp: "iCrazyTeddy", twitch: "icrazyteddy", twitchapp: "icrazyteddy", instagram: " ", instagramapp: " ", snapchat: " ", snapchatapp: " ", youtube: "iCrazyTeddy", youtubeINTRO: "5rGRi-iZLHo", youtubeapp: "iCrazyTeddy")
        crazyteddy.frontimage = UIImage(named: "crazyteddy")!
        members.append(crazyteddy)
        
        let adrive = Members(name: "adrive", job: "Streamer/Director", twitter: "aDrive_tK", twitterapp: "aDrive_tK", twitch: "adrive", twitchapp: "adrive", instagram: " ", instagramapp: " ", snapchat: " ", snapchatapp: " ", youtube: "xaDr1v3", youtubeINTRO: "d7wnwvMLJM8", youtubeapp: "xaDr1v3")
        adrive.frontimage = UIImage(named: "adrive")!
        members.append(adrive)
        let happy = Members(name: "happy", job: "COD: Player", twitter: "nichsuda", twitterapp: "nichsuda", twitch: "happyy97", twitchapp: "happyy97", instagram: " ", instagramapp: " ", snapchat: " ", snapchatapp: " ", youtube: " ", youtubeINTRO: " ", youtubeapp: " ")
        happy.frontimage = UIImage(named: "happyfront")!
        members.append(happy)
        let colechan = Members(name: "colechan", job: "COD: Player", twitter: "TSMColeChan", twitterapp: "TSMColeChan", twitch: "colechan827", twitchapp: "colechan827", instagram: " ", instagramapp: " ", snapchat: " ", snapchatapp: " ", youtube: " ", youtubeINTRO: " ", youtubeapp: " ")
        colechan.frontimage = UIImage(named: "colechanfront")!
        members.append(colechan)
        
        let goonjar = Members(name: "goonjar", job: "COD: Player", twitter: "Goonjar", twitterapp: "Goonjar", twitch: "goonjar", twitchapp: "goonjar", instagram: " ", instagramapp: " ", snapchat: " ", snapchatapp: " ", youtube: " ", youtubeINTRO: "e9d_xkmg1hs", youtubeapp: " ")
        goonjar.frontimage = UIImage(named: "goonjar")!
        members.append(goonjar)
        

        
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return members.count
    }
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let Cell = UITableViewCell()
        let member = self.members[indexPath.row]
        Cell.textLabel?.text = member.name
        return Cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let members = self.members[(indexPath as NSIndexPath).row]
        self.performSegue(withIdentifier: "MemberSegue", sender: members)
        
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let ControlME2 = segue.destination as! FifthViewController
        ControlME2.member = sender as! Members
        
    }
}

